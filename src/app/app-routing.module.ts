import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateClienteComponent } from './create-cliente/create-cliente.component';
import { ClienteDetailsComponent } from './cliente-details/cliente-details.component';
import { ClienteListComponent } from './cliente-list/cliente-list.component';
import { CreateCamareroComponent } from './create-camarero/create-camarero.component';
import { CamareroDetailsComponent } from './camarero-details/camarero-details.component';
import { CamareroListComponent } from './camarero-list/camarero-list.component';
import { CreateCocineroComponent } from './create-cocinero/create-cocinero.component';
import { CocineroDetailsComponent } from './cocinero-details/cocinero-details.component';
import { CocineroListComponent } from './cocinero-list/cocinero-list.component';
import { CreateMesaComponent } from './create-mesa/create-mesa.component';
import { MesaDetailsComponent } from './mesa-details/mesa-details.component';
import { MesaListComponent } from './mesa-list/mesa-list.component';
import { CreateFacturaComponent } from './create-factura/create-factura.component';
const routes: Routes = [
  { path: '', redirectTo: 'cliente', pathMatch: 'full' },
  { path: 'clientes', component: ClienteListComponent },
  { path: 'cliente/add', component: CreateClienteComponent },
  { path: 'cliente/edit/:id', component: ClienteDetailsComponent },
  { path: 'camareros', component: CamareroListComponent },
  { path: 'camarero/add', component: CreateCamareroComponent },
  { path: 'camarero/edit/:id', component: CamareroDetailsComponent },
  { path: 'cocineros', component: CocineroListComponent },
  { path: 'cocinero/add', component: CreateCocineroComponent },
  { path: 'cocinero/edit/:id', component: CocineroDetailsComponent },
  { path: 'mesas', component: MesaListComponent },
  { path: 'mesa/add', component: CreateMesaComponent },
  { path: 'mesa/edit/:id', component: MesaDetailsComponent },
  { path: 'factura', component: CreateFacturaComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
