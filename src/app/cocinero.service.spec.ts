import { TestBed } from '@angular/core/testing';

import { CocineroService } from './cocinero.service';

describe('CocineroService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CocineroService = TestBed.get(CocineroService);
    expect(service).toBeTruthy();
  });
});
