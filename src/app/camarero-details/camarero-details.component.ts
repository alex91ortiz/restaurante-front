import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { ActivatedRoute } from "@angular/router";
import { CamareroService } from '../camarero.service';
import { CamareroListComponent } from '../camarero-list/camarero-list.component';
import { Camarero } from './../camarero';

@Component({
  selector: 'app-camarero-details',
  templateUrl: './camarero-details.component.html',
  styleUrls: ['./camarero-details.component.css'],
  providers: [CamareroListComponent]
})
export class CamareroDetailsComponent implements OnInit {

  camarero = {};
  submitted = false;
  error = false;
  id = null;

  constructor(private camareroService: CamareroService, private listComponent: CamareroListComponent, private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get("id")
    
    this.camareroService.getCamarero(this.id).subscribe(res=>this.camarero = res);
    
  }
  

  save() {
    this.camareroService.updateCamarero(this.id, this.camarero)
      .subscribe(data => this.submitted = true, error => this.error = true);
    
  }

  onSubmit() {

    this.save();
  }
}
