import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CamareroDetailsComponent } from './camarero-details.component';

describe('CamareroDetailsComponent', () => {
  let component: CamareroDetailsComponent;
  let fixture: ComponentFixture<CamareroDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CamareroDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CamareroDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
