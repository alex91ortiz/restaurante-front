import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { CamareroService } from "./../camarero.service";
import { Camarero } from "./../camarero";

@Component({
  selector: 'app-camarero-list',
  templateUrl: './camarero-list.component.html',
  styleUrls: ['./camarero-list.component.css']
})

export class CamareroListComponent implements OnInit {

  displayedColumns: string[] = ['nombre', 'apellido1', 'apellido2','action'];

  camareros: Observable<Camarero[]>;

  constructor(private camareroService: CamareroService) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.camareros = this.camareroService.getCamarerosList();
  }

  deleteCamarero(id: number) {
    this.camareroService.deleteCamarero(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }
}
