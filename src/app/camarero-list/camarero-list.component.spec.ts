import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CamareroListComponent } from './camarero-list.component';

describe('CamareroListComponent', () => {
  let component: CamareroListComponent;
  let fixture: ComponentFixture<CamareroListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CamareroListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CamareroListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
