import { Component, OnInit } from '@angular/core';
import { ClienteService } from './../cliente.service';
import { Cliente } from './../cliente';
@Component({
  selector: 'app-create-cliente',
  templateUrl: './create-cliente.component.html',
  styleUrls: ['./create-cliente.component.css']
})
export class CreateClienteComponent implements OnInit {

  cliente: Cliente = new Cliente();
  submitted = false;
  error = false;

  constructor(private clienteService: ClienteService) { }

  ngOnInit() {
  }

  newCliente(): void {
    this.submitted = false;
    this.cliente = new Cliente();
  }

  save() {
    this.clienteService.createCliente(this.cliente)
      .subscribe(data => this.submitted = true, error => this.error = true);
    this.cliente = new Cliente();
  }

  onSubmit() {
    
    this.save();
  }
}
