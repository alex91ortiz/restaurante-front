import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { MesaService } from "./../mesa.service";
import { Mesa } from "./../mesa";

@Component({
  selector: 'app-mesa-list',
  templateUrl: './mesa-list.component.html',
  styleUrls: ['./mesa-list.component.css']
})

export class MesaListComponent implements OnInit {

  displayedColumns: string[] = ['numeromaxcomensales', 'ubicacion','action'];

  mesas: Observable<Mesa[]>;

  constructor(private mesaService: MesaService) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.mesas = this.mesaService.getMesasList();
  }

  deleteMesa(id: number) {
    this.mesaService.deleteMesa(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }
}
