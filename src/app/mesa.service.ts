import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MesaService {

  private baseUrl = '/mesa/mesa';

  constructor(private http: HttpClient) { }

  getMesa(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createMesa(Mesa: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, Mesa);
  }

  updateMesa(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteMesa(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getMesasList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
