import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { CocineroService } from "./../cocinero.service";
import { Cocinero } from "./../cocinero";

@Component({
  selector: 'app-cocinero-list',
  templateUrl: './cocinero-list.component.html',
  styleUrls: ['./cocinero-list.component.css']
})

export class CocineroListComponent implements OnInit {

  displayedColumns: string[] = ['nombre', 'apellido1', 'apellido2','action'];

  cocineros: Observable<Cocinero[]>;

  constructor(private cocineroService: CocineroService) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.cocineros = this.cocineroService.getCocinerosList();
  }

  deleteCocinero(id: number) {
    this.cocineroService.deleteCocinero(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }
}
