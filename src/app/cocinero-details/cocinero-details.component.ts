import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { ActivatedRoute } from "@angular/router";
import { CocineroService } from '../cocinero.service';
import { CocineroListComponent } from '../cocinero-list/cocinero-list.component';
import { Cocinero } from './../cocinero';

@Component({
  selector: 'app-cocinero-details',
  templateUrl: './cocinero-details.component.html',
  styleUrls: ['./cocinero-details.component.css'],
  providers: [CocineroListComponent]
})
export class CocineroDetailsComponent implements OnInit {

  cocinero = {};
  submitted = false;
  error = false;
  id = null;

  constructor(private cocineroService: CocineroService, private listComponent: CocineroListComponent, private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get("id")
    
    this.cocineroService.getCocinero(this.id).subscribe(res=>this.cocinero = res);
    
  }
  

  save() {
    this.cocineroService.updateCocinero(this.id, this.cocinero)
      .subscribe(data => this.submitted = true, error => this.error = true);
    
  }

  onSubmit() {

    this.save();
  }
}
