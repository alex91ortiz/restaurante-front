import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CocineroDetailsComponent } from './cocinero-details.component';

describe('CocineroDetailsComponent', () => {
  let component: CocineroDetailsComponent;
  let fixture: ComponentFixture<CocineroDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CocineroDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CocineroDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
