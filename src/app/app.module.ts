import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatStepperModule } from '@angular/material/stepper';


import { FormsModule ,ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';


import { CreateClienteComponent } from './create-cliente/create-cliente.component';
import { ClienteDetailsComponent } from './cliente-details/cliente-details.component';
import { ClienteListComponent } from './cliente-list/cliente-list.component';
import { CreateCocineroComponent } from './create-cocinero/create-cocinero.component';
import { CocineroListComponent } from './cocinero-list/cocinero-list.component';
import { CocineroDetailsComponent } from './cocinero-details/cocinero-details.component';
import { CamareroListComponent } from './camarero-list/camarero-list.component';
import { CamareroDetailsComponent } from './camarero-details/camarero-details.component';
import { CreateCamareroComponent } from './create-camarero/create-camarero.component';
import { MesaListComponent } from './mesa-list/mesa-list.component';
import { MesaDetailsComponent } from './mesa-details/mesa-details.component';
import { CreateMesaComponent } from './create-mesa/create-mesa.component';
import { FacturaListComponent } from './factura-list/factura-list.component';
import { CreateFacturaComponent } from './create-factura/create-factura.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateClienteComponent,
    ClienteDetailsComponent,
    ClienteListComponent,
    CreateCocineroComponent,
    CocineroListComponent,
    CocineroDetailsComponent,
    CamareroListComponent,
    CamareroDetailsComponent,
    CreateCamareroComponent,
    MesaListComponent,
    MesaDetailsComponent,
    CreateMesaComponent,
    FacturaListComponent,
    CreateFacturaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatRadioModule,
    MatTableModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatStepperModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FlexLayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
