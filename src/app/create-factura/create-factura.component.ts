import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from "rxjs";
import { FacturaService } from './../factura.service';
import { CamareroService } from "./../camarero.service";
import { CocineroService } from "./../cocinero.service";
import { ClienteService } from "./../cliente.service";
import { MesaService } from "./../mesa.service";
import { Factura } from './../factura';
import { Cliente } from './../cliente';
import { Camarero } from '../camarero';
import { Cocinero } from '../cocinero';
import { Mesa } from '../mesa';

@Component({
  selector: 'app-create-factura',
  templateUrl: './create-factura.component.html',
  styleUrls: ['./create-factura.component.css'],
})
export class CreateFacturaComponent implements OnInit {

  factura: Factura = new Factura();
  cliente: Cliente = new Cliente();
  camareros: Observable<Camarero[]>;
  cocineros: Observable<Cocinero[]>;
  mesas: Observable<Mesa[]>;
  detallefacturas = [];
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  threeFormGroup: FormGroup;
  isLinear = true;

  submitted = false;
  error = false;

  constructor(private _formBuilder: FormBuilder,
    private facturaService: FacturaService,
    private camareroService: CamareroService,
    private cocineroService: CocineroService,
    private clienteService: ClienteService,
    private mesaService: MesaService) { }

  ngOnInit() {
    this.camareros = this.camareroService.getCamarerosList();
    this.cocineros = this.cocineroService.getCocinerosList();
    this.mesas = this.mesaService.getMesasList();
    this.firstFormGroup = this._formBuilder.group({
      nombre: ['', Validators.required],
      apellido1: ['', Validators.required],
      apellido2: ['', Validators.required],
      observacion: ['', '']
    });
    this.secondFormGroup = this._formBuilder.group({
      camarero: ['', Validators.required],
      mesa: ['', Validators.required]
    });
    this.threeFormGroup = this._formBuilder.group({
      plato: ['', Validators.required],
      importe: ['', Validators.required],
      cocinero: ['', Validators.required]
    });
  }

  

  save() {
    this.factura.cliente = this.firstFormGroup.value;
    this.factura.camarero = this.secondFormGroup.value.camarero.id;
    this.factura.mesa = this.secondFormGroup.value.mesa.id;
    this.factura.detallefactura = this.detallefacturas;
    this.factura.fechaFactura = new Date();
    
    
    this.facturaService.createFactura(this.factura)
      .subscribe(data => this.submitted = true, error => this.error = true)
    this.factura = new Factura();
  }

  onSubmit(stepper) {

    this.save();
    stepper.reset();
  }

  onAdd() {
    console.log(this.threeFormGroup.value);
    if (this.threeFormGroup.value.plato != "" && this.threeFormGroup.value.importe != "" && this.threeFormGroup.value.cocinero != "") {
      this.detallefacturas.push(this.threeFormGroup.value);
      this.threeFormGroup = this._formBuilder.group({
        plato: ['', Validators.required],
        importe: ['', Validators.required],
        cocinero: ['', Validators.required]
      });
    }
  }
}
