import { Component, OnInit } from '@angular/core';
import { CamareroService } from './../camarero.service';
import { Camarero } from './../camarero';
@Component({
  selector: 'app-create-camarero',
  templateUrl: './create-camarero.component.html',
  styleUrls: ['./create-camarero.component.css']
})
export class CreateCamareroComponent implements OnInit {

  camarero: Camarero = new Camarero();
  submitted = false;
  error = false;

  constructor(private camareroService: CamareroService) { }

  ngOnInit() {
  }

  newCamarero(): void {
    this.submitted = false;
    this.camarero = new Camarero();
  }

  save() {
    this.camareroService.createCamarero(this.camarero)
      .subscribe(data => this.submitted = true, error => this.error = true);
    this.camarero = new Camarero();
  }

  onSubmit() {
    
    this.save();
  }
}
