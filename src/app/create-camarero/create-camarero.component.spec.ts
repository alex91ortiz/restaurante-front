import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCamareroComponent } from './create-camarero.component';

describe('CreateCamareroComponent', () => {
  let component: CreateCamareroComponent;
  let fixture: ComponentFixture<CreateCamareroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCamareroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCamareroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
