import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CocineroService {

  private baseUrl = '/cocinero/cocinero';

  constructor(private http: HttpClient) { }

  getCocinero(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createCocinero(Cocinero: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, Cocinero);
  }

  updateCocinero(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteCocinero(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getCocinerosList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
