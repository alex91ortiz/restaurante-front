import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { ActivatedRoute } from "@angular/router";
import { ClienteService } from '../cliente.service';
import { ClienteListComponent } from '../cliente-list/cliente-list.component';
import { Cliente } from './../cliente';

@Component({
  selector: 'app-cliente-details',
  templateUrl: './cliente-details.component.html',
  styleUrls: ['./cliente-details.component.css'],
  providers: [ClienteListComponent]
})
export class ClienteDetailsComponent implements OnInit {

  cliente = {};
  submitted = false;
  error = false;
  id = null;

  constructor(private clienteService: ClienteService, private listComponent: ClienteListComponent, private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get("id")
    
    this.clienteService.getCliente(this.id).subscribe(res=>this.cliente = res);
    
  }
  

  save() {
    this.clienteService.updateCliente(this.id, this.cliente)
      .subscribe(data => this.submitted = true, error => this.error = true);
    
  }

  onSubmit() {

    this.save();
  }
}
