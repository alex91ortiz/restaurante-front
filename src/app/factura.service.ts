import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FacturaService {

  private baseUrl = '/factura/factura';

  constructor(private http: HttpClient) { }

  getFactura(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createFactura(Factura: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, Factura);
  }

  updateFactura(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteFactura(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getFacturasList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
}
