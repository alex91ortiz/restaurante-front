import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCocineroComponent } from './create-cocinero.component';

describe('CreateCocineroComponent', () => {
  let component: CreateCocineroComponent;
  let fixture: ComponentFixture<CreateCocineroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCocineroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCocineroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
