import { Component, OnInit } from '@angular/core';
import { CocineroService } from './../cocinero.service';
import { Cocinero } from './../cocinero';
@Component({
  selector: 'app-create-cocinero',
  templateUrl: './create-cocinero.component.html',
  styleUrls: ['./create-cocinero.component.css']
})
export class CreateCocineroComponent implements OnInit {

  cocinero: Cocinero = new Cocinero();
  submitted = false;
  error = false;

  constructor(private cocineroService: CocineroService) { }

  ngOnInit() {
  }

  newCocinero(): void {
    this.submitted = false;
    this.cocinero = new Cocinero();
  }

  save() {
    this.cocineroService.createCocinero(this.cocinero)
      .subscribe(data => this.submitted = true, error => this.error = true);
    this.cocinero = new Cocinero();
  }

  onSubmit() {
    
    this.save();
  }
}
