import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { ClienteService } from "./../cliente.service";
import { Cliente } from "./../cliente";

@Component({
  selector: 'app-cliente-list',
  templateUrl: './cliente-list.component.html',
  styleUrls: ['./cliente-list.component.css']
})

export class ClienteListComponent implements OnInit {

  displayedColumns: string[] = ['nombre', 'apellido1', 'apellido2', 'observacion','action'];

  clientes: Observable<Cliente[]>;

  constructor(private clienteService: ClienteService) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.clientes = this.clienteService.getClientesList();
  }

  deleteCliente(id: number) {
    this.clienteService.deleteCliente(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }
}
