export class Cliente {
    id: number;
    nombre: string;
    apellido1: string;
    apellido2: string;
    observacion: string;
}