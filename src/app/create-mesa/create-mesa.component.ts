import { Component, OnInit } from '@angular/core';
import { MesaService } from './../mesa.service';
import { Mesa } from './../mesa';
@Component({
  selector: 'app-create-mesa',
  templateUrl: './create-mesa.component.html',
  styleUrls: ['./create-mesa.component.css']
})
export class CreateMesaComponent implements OnInit {

  mesa: Mesa = new Mesa();
  submitted = false;
  error = false;

  constructor(private mesaService: MesaService) { }

  ngOnInit() {
  }

  newMesa(): void {
    this.submitted = false;
    this.mesa = new Mesa();
  }

  save() {
    this.mesaService.createMesa(this.mesa)
      .subscribe(data => this.submitted = true, error => this.error = true);
    this.mesa = new Mesa();
  }

  onSubmit() {
    
    this.save();
  }
}
