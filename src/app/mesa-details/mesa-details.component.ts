import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { ActivatedRoute } from "@angular/router";
import { MesaService } from '../mesa.service';
import { MesaListComponent } from '../mesa-list/mesa-list.component';
import { Mesa } from './../mesa';

@Component({
  selector: 'app-mesa-details',
  templateUrl: './mesa-details.component.html',
  styleUrls: ['./mesa-details.component.css'],
  providers: [MesaListComponent]
})
export class MesaDetailsComponent implements OnInit {

  mesa = {};
  submitted = false;
  error = false;
  id = null;

  constructor(private mesaService: MesaService, private listComponent: MesaListComponent, private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get("id")
    
    this.mesaService.getMesa(this.id).subscribe(res=>this.mesa = res);
    
  }
  

  save() {
    this.mesaService.updateMesa(this.id, this.mesa)
      .subscribe(data => this.submitted = true, error => this.error = true);
    
  }

  onSubmit() {

    this.save();
  }
}
