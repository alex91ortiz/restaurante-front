import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesaDetailsComponent } from './mesa-details.component';

describe('MesaDetailsComponent', () => {
  let component: MesaDetailsComponent;
  let fixture: ComponentFixture<MesaDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesaDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesaDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
